# README #

### What is this repository for? ###

* To add active users based on profile to a particular public group

### How do I get set up? ###

* Open `Developer Console`.
* Press `Ctrl+E` to open the Execute Anonymous Window.
* Copy the below code in the window:

~~~~
	List<GroupMember> groupList = new List<GroupMember>();
	Group publicGroup = [SELECT Id FROM Group WHERE DeveloperName='add_public_group_api_name']; 	
	String profileId = [SELECT Id, Name FROM Profile WHERE Name = 'add_profile_name'][0].Id;	
	List<User> userList=[SELECT Id, ProfileId FROM User WHERE ProfileId =: profileId AND IsActive=true]; 
	for(User userRecord : userList) {	
		GroupMember groupMember = new GroupMember();
		groupMember.groupId = publicGroup.Id;
		groupMember.UserOrGroupId = userRecord.Id;
		groupList.add(groupMember);
	} 
	if (groupList.size()>0) {			
		Database.INSERT(groupList, false);	
	}
~~~~
* Replace the `add_public_group_api_name` & `add_profile_name` with respective values. 
* Click on `Execute` button.